package realTimeBoardTest;

import oshi.SystemInfo;

import java.io.IOException;
import java.util.*;

class TestClass {
    //Колличество клиентов
    private int clientCol;
    //Каждые n миллисекунд в m потоков отправляют данные, т.е. колличество запросов в секунду приблизительно равно m*n*1000
    private int intervalMillis;
    private int numThreadSender;
    private int testExecutionTimeMillis;
    private long startTimeTest;
    private Random random = new Random();
    private final List<String> messageList = new LinkedList<>();
    private final Map<String, List<Long>> responseClientMap = new HashMap<>();
    private List<Thread> senderThreadList = new ArrayList<>();
    private boolean enableLogging;
    private Thread intermediateMonitoringThread;
    private SystemInfo systemInfo;
    private StressTestResult stressTestResult;
    private Integer numcols;

    TestClass(int clientCol, int intervalMillis, int numThreadSender, int testExecutionTimeMillis, int numcols, boolean enableLogging) {
        this.clientCol = clientCol;
        this.intervalMillis = intervalMillis;
        this.numThreadSender = numThreadSender;
        this.testExecutionTimeMillis = testExecutionTimeMillis;
        for (int i = 0; i < clientCol; i++) {
            responseClientMap.put(String.valueOf(i), new LinkedList<>());
        }
        this.enableLogging = enableLogging;
        this.systemInfo = new SystemInfo();
        this.stressTestResult = new StressTestResult();
        this.numcols = numcols;
    }

    StressTestResult runStressTest() throws IOException, InterruptedException {
        startTimeTest = new Date().getTime();
        TcpChatServer server = new TcpChatServer("server", 7777, numcols);
        server.start();

        List<TcpChatClient> clientList = new ArrayList<>();
        for (int i = 0; i < clientCol; i++) {
            int finalI = i;
            TcpChatClient client = new TcpChatClient("client_" + finalI, 7777, message -> {
                String numberClient = String.valueOf(finalI);
                if (!"Введите имя пользователя:".equals(message)) {
                    String[] messageColon = message.split(":");
                    String sendTime = null;
                    try {
                        sendTime = messageColon[1];
                    } catch (Exception e) {
                        System.out.println();
                    }

                    synchronized (responseClientMap.get(numberClient)) {
                        try {
                            responseClientMap.get(numberClient).add(new Date().getTime() - Long.parseLong(sendTime));
                        } catch (Exception e) {
                            System.out.println();
                        }

                    }
                }
            });
            client.start();
            clientList.add(client);
        }

        Thread.sleep(10000);

        int i = 0;
        for (TcpChatClient current : clientList) {
            current.sendMessage("client_" + String.valueOf(i));
            i++;
        }

        Thread.sleep(1000);

        for (i = 0; i < numThreadSender; i++) {
            senderThreadList.add(new Thread(() -> {
                try {
                    while (!Thread.currentThread().isInterrupted()) {
                        Thread.sleep(intervalMillis);
                        Integer clientNum = random.nextInt(clientCol);
                        String message = String.valueOf(new Date().getTime());

                        synchronized (messageList) {
                            messageList.add(message);
                        }
                        synchronized (responseClientMap.get(String.valueOf(clientNum))) {
                            responseClientMap.get(String.valueOf(clientNum)).add(0L);
                        }

                        clientList.get(clientNum).sendMessage(message);
                    }
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                }
            }));
        }
        for (Thread current : senderThreadList) {
            current.start();
        }

        Thread finishingCheckThread = new Thread(() -> {
            boolean isDoneInterrupt = false;
            while (!Thread.currentThread().isInterrupted()) {
                if ((new Date().getTime() - startTimeTest) > testExecutionTimeMillis) {
                    if (!isDoneInterrupt) {
                        for (Thread current : senderThreadList) {
                            current.interrupt();
                        }
                        isDoneInterrupt = true;
                    }

                    boolean isTerminated = true;
                    for (Thread current : senderThreadList) {
                        if (!current.getState().equals(Thread.State.TERMINATED)) {
                            isTerminated = false;
                        }
                    }

                    if (isTerminated) {
                        boolean bool = true;
                        for (Map.Entry<String, List<Long>> current : responseClientMap.entrySet()) {
                            if (messageList.size() != current.getValue().size()) {
                                bool = false;
                                break;
                            }
                        }

                        if (bool) {
                            break;
                        }
                    }
                }
            }
        });
        finishingCheckThread.start();
        log();

        finishingCheckThread.join();

        if (enableLogging) {
            System.out.println("Время выполнения теста: " + (new Date().getTime() - startTimeTest));
            System.out.println("Колличество пакетов: " + messageList.size());
        }

        for (TcpChatClient current : clientList) {
            current.stop();
        }
        server.stop();
        intermediateMonitoringThread.interrupt();

        stressTestResult.setResponseClientMap(responseClientMap);
        return stressTestResult.calculateResult();
    }

    private void log() {
        intermediateMonitoringThread = new Thread(() -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Thread.sleep(1000);

                    int j = 0;
                    for (Map.Entry<String, List<Long>> current : responseClientMap.entrySet()) {
                        j = messageList.size() - current.getValue().size() + j;
                    }
                    if (enableLogging) {
                        System.out.println("Осталось загрузить:" + j);
                        System.out.println("System cpu load:" + systemInfo.getHardware().getProcessor().getSystemCpuLoad());
                        System.out.println("Get total memory:" + systemInfo.getHardware().getMemory().getAvailable());
                    }
                    stressTestResult.putCpuLoad(systemInfo.getHardware().getProcessor().getSystemCpuLoad());
                    stressTestResult.putTotalMemory(systemInfo.getHardware().getMemory().getAvailable());
                }
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        });
        intermediateMonitoringThread.start();
    }
}