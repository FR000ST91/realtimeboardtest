package realTimeBoardTest;

interface FinalStringsTest {
    public final String TCP_CLIPPED_MESSAGE_TEST = "test1\r\ntest2\r\ntest3";
    public final String HELP_MESSAGE = "help Описание пользовательских команд\r\n" +
            "numberOfConnection Возвращает колличество подключенных пользователей\r\n" +
            "rename Переименовывает текущего пользователя";
}
