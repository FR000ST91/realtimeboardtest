package realTimeBoardTest;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class StressTestResult {
    private Double cpuLoadAverage;
    private Double availableMemoryAverage;
    public Double responseTimeAverage;
    private List<Double> cpuLoadTicks = new LinkedList<>();
    private List<Long> availableMemoryTicks = new LinkedList<>();
    private Map<String, List<Long>> responseClientMap;

    StressTestResult() {
        cpuLoadTicks = new LinkedList<>();
    }

    void putCpuLoad(Double in) {
        cpuLoadTicks.add(in);
    }

    void setResponseClientMap(Map<String, List<Long>> in) {
        this.responseClientMap = in;
    }

    void putTotalMemory(Long in) {
        this.availableMemoryTicks.add(in);
    }

    StressTestResult calculateResult() {
        cpuLoadAverage = cpuLoadTicks.stream().mapToDouble(Double::doubleValue).average().getAsDouble();
        availableMemoryAverage = availableMemoryTicks.stream().mapToLong(Long::longValue).average().getAsDouble() / (1024 * 1024);
        responseTimeAverage = 0D;
        for (List<Long> current : responseClientMap.values()) {
            Double averageFlow = current.stream().mapToLong(Long::longValue).average().getAsDouble();
            responseTimeAverage = responseTimeAverage + averageFlow;
        }
        responseTimeAverage = responseTimeAverage / responseClientMap.size();
        clean();
        return this;
    }

    private void clean() {
        cpuLoadTicks = null;
        availableMemoryTicks = null;
        responseClientMap = null;
    }

    @Override
    public String toString() {
        return "StressTestResult{" +
                "cpuLoadAverage=" + cpuLoadAverage +
                ", availableMemoryAverage=" + availableMemoryAverage +
                ", responseTimeAverage=" + responseTimeAverage +
                '}';
    }
}
