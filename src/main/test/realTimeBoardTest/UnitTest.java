package realTimeBoardTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UnitTest extends Assert implements FinalStrings, FinalStringsTest {
    private TcpCommand tcpCommand;
    private TcpChatServer tcpChatServer;
    private SelectionKey selectionKeyMock;

    @Before
    public void start() throws IOException {
        tcpCommand = new TcpCommand("realTimeBoardTest", null, 10) {
            @Override
            public String bodyCommand(List<String> params, SelectionKey key) {
                return params.toString();
            }
        };
        tcpChatServer = spy(new TcpChatServer("server", 0,1));
        doReturn(true).when(tcpChatServer).write(any(SelectionKey.class));
        doReturn("testUser").when(tcpChatServer).getThisUser(any(SelectionKey.class));
        selectionKeyMock = mock(SelectionKey.class);
    }

    @Test
    public void testGetResultCommand() {
        assertEquals(tcpCommand.getResult(("-realTimeBoardTest 1 2 3 4 5 6 7 8 9 10" + DELIMITER).getBytes(), null), "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]" + DELIMITER);
    }

    @Test
    public void isThisCommandTest() {
        assertEquals(tcpCommand.isThisCommand(("-realTimeBoardTest 1 2 3 4 5 6 7 8 9 10" + DELIMITER).getBytes()), true);
    }

    @Test
    public void tcpClippedMessageTest() {
        String clippedMessage = tcpChatServer.clippedMessage(selectionKeyMock, TCP_CLIPPED_MESSAGE_TEST);
        assertEquals(clippedMessage, String.format("test1%stest2%s", DELIMITER, DELIMITER));
        assertEquals(tcpChatServer.restoreMessageIfNeeded(selectionKeyMock, "test4"), "test3test4");
    }

    @Test
    public void checkAndRunTheCommandTest() throws IOException {
        assertEquals(tcpChatServer.checkAndRunTheCommand(selectionKeyMock, String.format("-help%s", DELIMITER).getBytes()), true);
        assertEquals(tcpChatServer.checkAndRunTheCommand(selectionKeyMock, String.format("help%s", DELIMITER).getBytes()), false);
    }

    /*@Test TOdo
    public void addMessageTest() {
        tcpChatServer.addMessage(mock(SelectionKey.class), "test".getBytes());
        assertEquals(tcpChatServer.messages.get(false).toString(), "[testUser:test]");
    }*/

    @Test
    public void authenticateUser() throws IOException {
        tcpChatServer.authenticateUser(selectionKeyMock, "testUser");
        assertEquals(tcpChatServer.users.get(selectionKeyMock), "testUser");
    }

    /*@Test TOdo
    public void getLastMessagesTest() {
        tcpChatServer.messages.get(true).add("test1");
        tcpChatServer.messages.get(true).add("test2");
        assertEquals(tcpChatServer.getLastMessages(), String.format("test1%stest2%s", DELIMITER, DELIMITER));
    }*/

    @Test
    public void getHelpDescriptionTest() {
        assertEquals(tcpChatServer.getHelpDescription(), HELP_MESSAGE);
    }
}
