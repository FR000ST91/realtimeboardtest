package realTimeBoardTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Todo необходимо отделить сервер от пользовательских сессий для мониторинга ресурсов только сервера
public class StressTesting extends Assert {
    private List<TestClass> testClassList = new ArrayList<>();

    @Before
    public void before() {
        testClassList.add(new TestClass(50, 10, 4, 30000, 1, false));
        testClassList.add(new TestClass(50, 10, 4, 30000, 10, false));
        testClassList.add(new TestClass(100, 10, 4, 30000, 1, false));
        testClassList.add(new TestClass(100, 10, 4, 30000, 10, false));
        testClassList.add(new TestClass(200, 10, 4, 30000, 1, false));
        testClassList.add(new TestClass(200, 10, 4, 30000, 10, false));
        testClassList.add(new TestClass(300, 10, 4, 30000, 1, false));
        testClassList.add(new TestClass(300, 10, 4, 30000, 10, false));
    }

    @Test
    public void test() throws IOException, InterruptedException {
        for (TestClass currentTest : testClassList) {
            StressTestResult result = currentTest.runStressTest();
            System.out.println(result.responseTimeAverage);
            Thread.sleep(1000);//Оставить, т.к. порт почему-то моментально не освобождается
        }
    }
}
