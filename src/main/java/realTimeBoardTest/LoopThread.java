package realTimeBoardTest;

abstract class LoopThread implements Runnable, FinalStrings {
    private final String id;
    private volatile boolean running;

    LoopThread(String id) {
        this.id = id;
    }

    public final synchronized void start() {
        Thread executionThread = new Thread(this, id);
        running = true;
        executionThread.start();
    }

    final void stop() {
        running = false;
    }

    public final void run() {
        while (running) {
            runLoop();
        }
        clean();
    }

    abstract void clean();

    abstract void runLoop();
}
