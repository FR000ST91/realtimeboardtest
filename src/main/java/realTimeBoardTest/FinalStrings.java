package realTimeBoardTest;

public interface FinalStrings {
    String DELIMITER = "\r\n";
    String USERNAME_IS_BUSY = "Имя пользователя: \"%s\" занято";
    String USERNAME_CHANGE_TO = "Имя пользователя изменено на:\"%s\"";
    String ENTER_YOUR_USERNAME = "Введите имя пользователя:" + DELIMITER;
    String USERNAME_CHANGE_TO__ENTER_YOUR_USERNAME = USERNAME_IS_BUSY + "!" + DELIMITER + "Введите имя пользователя:" + DELIMITER;
    String TWO_LINES_COLON = "%s:%s";
    String TWO_LINES_SPACER = "%s %s";
    String TCP_COMMAND_REGEX = "-%s(\\s\\w+){%s}";
    String SPACE = "\\s";
}
