package realTimeBoardTest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.nio.channels.SelectionKey.OP_ACCEPT;
import static java.nio.channels.SelectionKey.OP_READ;

public class TcpChatServer extends TcpChatComponent implements Server {
    Map<SelectionKey, String> users;
    private String helpDescription;
    private List<String> messages;
    private List<BaseCommand> commands;

    private int numThreads;
    private ExecutorService es;

    public TcpChatServer(String id, int port, int numThreads) throws IOException {
        super(id, port, numThreads);
        commands = commands();
        users = new HashMap<>();
        messages = new LinkedList<>();
        this.numThreads = numThreads;

        es = Executors.newFixedThreadPool(numThreads);
    }

    @Override
    public List<BaseCommand> commands() {
        List<BaseCommand> commands = new LinkedList<>();

        commands.add(new TcpCommand("help", "Описание пользовательских команд", 0) {
            @Override
            public String bodyCommand(List<String> params, SelectionKey key) {
                return getHelpDescription();
            }
        });
        commands.add(new TcpCommand("numberOfConnection", "Возвращает колличество подключенных пользователей", 0) {
            @Override
            public String bodyCommand(List<String> params, SelectionKey key) {
                return String.valueOf(users.size());
            }
        });
        commands.add(new TcpCommand("rename", "Переименовывает текущего пользователя", 1) {
            @Override
            public String bodyCommand(List<String> params, SelectionKey key) {
                String rezult;
                String reName = params.get(0);
                if (users.values().contains(reName)) {
                    rezult = String.format(USERNAME_IS_BUSY, reName);
                } else {
                    users.put(key, reName);
                    rezult = String.format(USERNAME_CHANGE_TO, reName);
                }
                return rezult;
            }
        });

        return commands;
    }

    @Override
    public void runLoop() {
        try {
            selector.select();
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            int i = 0;
            while (iterator.hasNext()) {
                final SelectionKey key = iterator.next();
                iterator.remove();

                int finalI = i;
                es.execute(() -> {
                            try {
                                if (!key.isValid()) {
                                    return;
                                }
                                if (key.isAcceptable()) {

                                    accept(key);

                                } else if (key.isReadable()) {
                                    read(key, finalI % numThreads);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                );
                i++;
            }
            es.shutdown();
            try {
                es.awaitTermination(10, TimeUnit.HOURS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            es = Executors.newFixedThreadPool(numThreads);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    boolean write(SelectionKey key) throws IOException {
        ByteBuffer buffer = (ByteBuffer) key.attachment();
        SocketChannel channel = (SocketChannel) key.channel();
        channel.write(buffer);
        key.interestOps(OP_READ);
        return true;
    }

    @Override
    void handleIncomingData(SelectionKey sender, byte[] data, int bufNum) throws IOException {
        boolean auth = isAuthUser(sender);
        boolean isCommand = checkAndRunTheCommand(sender, data);
        if (isCommand) {
            return;
        }
        if (auth) {
            addMessage(sender, data, bufNum);
        }

        circumventionOfKeys(sender, data, auth, bufNum);
    }

    private void circumventionOfKeys(SelectionKey sender, byte[] data, boolean auth, int bufNum) throws IOException {
        for (SelectionKey key : selector.keys()) {
            if (key.channel() instanceof ServerSocketChannel) {
                continue;
            }
            if (key.equals(sender)) {
                if (getThisUser(key) == null) {
                    String[] split = new String(data).split(DELIMITER);
                    authenticateUser(key, split[0]);
                    if (split.length > 1) {
                        circumventionOfKeys(sender, String.join(DELIMITER, Arrays.asList(Arrays.copyOfRange(split, 1, split.length))).getBytes(), auth, bufNum);
                    }
                    return;
                }
                continue;
            }
            if (getThisUser(key) == null) {
                continue;
            }

            if (auth) {
                synchronized (key) {
                    ByteBuffer newByteBuffer = ByteBuffer.wrap((String.join(DELIMITER, byteBufferMessage.get(bufNum).getValue()) + DELIMITER).getBytes());
                    key.attach(newByteBuffer);
                    write(key);
                }
            }
        }
        messages.addAll(byteBufferMessage.get(bufNum).getValue());
        byteBufferMessage.get(bufNum).getValue().clear();
    }

    @Override
    void breakConnection(SelectionKey key) {
        users.remove(key);
    }

    @Override
    String greetingMessage() {
        return ENTER_YOUR_USERNAME;
    }

    @Override
    public void clean() {
        synchronized (selector) {
            super.clean();
        }
    }

    private boolean isAuthUser(SelectionKey sender) {
        return users.get(sender) != null && !(sender.channel() instanceof ServerSocketChannel);
    }

    @Override
    public boolean checkAndRunTheCommand(SelectionKey sender, byte[] data) throws IOException {
        for (BaseCommand currentCommand : commands) {
            if (currentCommand.isThisCommand(data)) {
                String resultCommand = currentCommand.getResult(data, sender);
                sender.attach(ByteBuffer.wrap(resultCommand.getBytes()));
                write(sender);
                return true;
            }
        }
        return false;
    }

    String getThisUser(SelectionKey key) {
        return users.get(key);
    }

    private void addMessage(SelectionKey sender, byte[] data, int bufNum) {
        String split = new String(data);
        for (String current : split.split(DELIMITER)) {
            byteBufferMessage.get(bufNum).getValue().add(String.format(TWO_LINES_COLON, getThisUser(sender), current));
        }

    }

    boolean authenticateUser(SelectionKey key, String user) throws IOException {
        synchronized (this) {
            if (users.values().contains(user)) {
                key.attach(ByteBuffer.wrap(String.format(USERNAME_CHANGE_TO__ENTER_YOUR_USERNAME, user).getBytes()));
                write(key);
                return false;
            } else {
                users.put(key, user);
                String lastMessages = getLastMessages();
                if (lastMessages != null) {
                    key.attach(ByteBuffer.wrap(lastMessages.getBytes()));
                }
                write(key);
                return true;
            }
        }
    }

    private String getLastMessages() {
        if (!messages.isEmpty()) {
            return String.join(DELIMITER, messages.subList(messages.size() < 100 ? 0 : messages.size() - 100, messages.size())) + DELIMITER;
        }
        return null;
    }

    @Override
    protected Selector selector() throws IOException {
        AbstractSelector selector = SelectorProvider.provider().openSelector();
        channel.register(selector, OP_ACCEPT);
        return selector;
    }

    @Override
    protected AbstractSelectableChannel channel(InetSocketAddress address) throws IOException {
        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.configureBlocking(false);
        channel.socket().bind(address);
        return channel;
    }

    String getHelpDescription() {
        if (helpDescription == null) {
            helpDescription = String.join(DELIMITER, commands.stream().map(command -> String.format(TWO_LINES_SPACER, command.getCommandName(), command.getDescription())).collect(Collectors.toList()));
        }
        return helpDescription;
    }
}