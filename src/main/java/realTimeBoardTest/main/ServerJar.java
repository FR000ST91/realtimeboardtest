package realTimeBoardTest.main;

import realTimeBoardTest.TcpChatServer;

import java.io.IOException;

public class ServerJar {
    public static void main(String[] args) throws IOException {
        TcpChatServer server = new TcpChatServer("server", 7777,10);
        server.start();
    }
}
