package realTimeBoardTest.main;

import realTimeBoardTest.TcpChatClient;

import java.io.IOException;
import java.util.Scanner;

public class ClientJar {
    public static void main(String[] args) throws IOException {
        TcpChatClient client = new TcpChatClient("client", 7777, System.out::println);
        client.start();

        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                Scanner in = new Scanner(System.in);
                String message = in.next();
                client.sendMessage(message);
            }
        }).start();
    }
}
