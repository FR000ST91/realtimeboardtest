package realTimeBoardTest;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.List;

interface Server {
    boolean checkAndRunTheCommand(SelectionKey sender, byte[] data) throws IOException;

    List<BaseCommand> commands();
}
