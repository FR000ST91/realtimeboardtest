package realTimeBoardTest;

import javafx.util.Pair;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.*;

import static java.lang.System.arraycopy;
import static java.nio.channels.SelectionKey.OP_READ;
import static java.nio.channels.SelectionKey.OP_WRITE;

abstract class NioChatComponent extends LoopThread {
    final Selector selector;
    List<Pair<ByteBuffer, List<String>>> byteBufferMessage;
    final AbstractSelectableChannel channel;
    Map<SelectionKey, String> remnantsOfMessagesMap = new HashMap<>();
    private int numThreads;

    NioChatComponent(String id, int port, int numThreads) throws IOException {
        this(id, new InetSocketAddress(port));
        this.numThreads = numThreads;
        this.byteBufferMessage = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            byteBufferMessage.add(new Pair<>(ByteBuffer.allocate(8192), new LinkedList<>()));
        }
    }

    private NioChatComponent(String id, InetSocketAddress address) throws IOException {
        super(id);
        this.channel = channel(address);
        this.selector = selector();
    }

    protected abstract Selector selector() throws IOException;

    protected abstract AbstractSelectableChannel channel(InetSocketAddress address) throws IOException;

    abstract boolean write(SelectionKey key) throws IOException;

    abstract void handleIncomingData(SelectionKey key, byte[] data, int bufNum) throws IOException;

    abstract void breakConnection(SelectionKey key);

    abstract String greetingMessage();

    abstract String restoreMessageIfNeeded(SelectionKey key, String in);

    abstract String clippedMessage(SelectionKey key, String in);

    void connect(SelectionKey selectionKey) throws IOException {
        SocketChannel channel = (SocketChannel) selectionKey.channel();
        try {
            channel.finishConnect();
            channel.configureBlocking(false);
            channel.register(selector, OP_WRITE);
        } catch (IOException e) {
            e.printStackTrace();
            selectionKey.channel().close();
            selectionKey.cancel();
        }
    }

    void accept(SelectionKey selectionKey) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) selectionKey.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        SelectionKey key = socketChannel.register(selector, OP_READ);
        if (greetingMessage() != null) {
            key.attach(ByteBuffer.wrap(greetingMessage().getBytes()));
        }

        //Todo
        /*String body = "HTTP/1.1 200 OK\n\n" +
                "<html><head><meta charset=\"UTF-8\"><title>Title</title></head><body>TEST</body></html>";
        key.attach(ByteBuffer.wrap(body.getBytes()));*/

        write(key);
    }

    void read(SelectionKey selectionKey, int bufNum) throws IOException {
        ByteBuffer usedByteBuffer = byteBufferMessage.get(bufNum).getKey();
        String result;
        synchronized (usedByteBuffer) {
            SocketChannel channel = (SocketChannel) selectionKey.channel();
            usedByteBuffer.clear();
            int readCount;

            try {
                readCount = channel.read(usedByteBuffer);
            } catch (IOException e) {
                selectionKey.cancel();
                channel.close();
                return;
            }

            if (readCount == -1) {
                selectionKey.channel().close();
                selectionKey.cancel();
                breakConnection(selectionKey);
                return;
            }

            byte[] data = new byte[usedByteBuffer.position()];
            arraycopy(usedByteBuffer.array(), 0, data, 0, usedByteBuffer.position());
            result = restoreMessageIfNeeded(selectionKey, new String(data));
            handleIncomingData(selectionKey, clippedMessage(selectionKey, result).getBytes(), bufNum);
        }
    }

    public void clean() {
        for (SelectionKey key : selector.keys()) {
            close(key.channel());
        }
        close(selector);
    }

    private static void close(Closeable... closeables) {
        try {
            for (Closeable closeable : closeables)
                closeable.close();
        } catch (IOException e) {
            //
        }
    }
}
