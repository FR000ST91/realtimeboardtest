package realTimeBoardTest;

import java.nio.channels.SelectionKey;
import java.util.List;

abstract class BaseCommand {
    private final String commandName;
    private final String description;

    BaseCommand(String commandName, String description) {
        this.commandName = commandName;
        this.description = description;
    }

    public abstract String bodyCommand(List<String> params, SelectionKey key);

    abstract String getResult(byte[] in, SelectionKey key);

    abstract boolean isThisCommand(byte[] in);

    String getCommandName() {
        return commandName;
    }

    String getDescription() {
        return description;
    }
}
