package realTimeBoardTest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Iterator;
import java.util.LinkedList;

import static java.nio.channels.SelectionKey.*;

public class TcpChatClient extends TcpChatComponent implements Client {
    private final LinkedList<String> messages;
    private final ChatResponseHandler handler;

    public TcpChatClient(String id, int port, ChatResponseHandler handler) throws IOException {
        super(id, port, 1);
        this.messages = new LinkedList<>();
        this.handler = handler;
    }

    @Override
    public void runLoop() {
        try {
            selector.select(1);

            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();

            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();

                if (!key.isValid()) {
                    continue;
                }

                if (key.isConnectable()) {
                    connect(key);

                } else if (key.isWritable()) {
                    write(key);
                } else if (key.isReadable()) {
                    read(key, 0);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    boolean write(SelectionKey selectionKey) throws IOException {
        SocketChannel channel = (SocketChannel) selectionKey.channel();
        synchronized (messages) {
            while (!messages.isEmpty()) {
                String message = messages.poll();
                channel.write(ByteBuffer.wrap(message.getBytes()));
            }
        }
        selectionKey.interestOps(OP_READ);
        return true;
    }

    @Override
    void handleIncomingData(SelectionKey selectionKey, byte[] data, int bufNum) throws IOException {
        for (String message : new String(data).split(DELIMITER)) {
            handler.onMessage(message);
        }
    }

    @Override
    void breakConnection(SelectionKey key) {

    }

    @Override
    String greetingMessage() {
        return null;
    }

    @Override
    protected Selector selector() throws IOException {
        Selector selector = Selector.open();
        channel.register(selector, OP_CONNECT);
        return selector;
    }

    @Override
    protected AbstractSelectableChannel channel(InetSocketAddress address) throws IOException {
        SocketChannel channel = SocketChannel.open();
        channel.configureBlocking(false);
        channel.connect(address);
        return channel;
    }

    public void sendMessage(String message) {
        synchronized (messages) {
            messages.add(message + DELIMITER);
        }
        SelectionKey key = channel.keyFor(selector);
        key.interestOps(OP_WRITE);

    }
}
