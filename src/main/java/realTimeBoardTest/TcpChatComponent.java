package realTimeBoardTest;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.Arrays;

abstract class TcpChatComponent extends NioChatComponent {
    TcpChatComponent(String id, int port, int numThreads) throws IOException {
        super(id, port, numThreads);
    }

    String clippedMessage(SelectionKey key, String in) {
        String result = in;
        if (!result.endsWith(DELIMITER)) {
            String[] devide = result.split(DELIMITER);
            result = String.join(DELIMITER, Arrays.copyOfRange(devide, 0, devide.length - 1)) + DELIMITER;
            String stump = devide[devide.length - 1];
            remnantsOfMessagesMap.put(key, stump);
        }
        return result;
    }

    String restoreMessageIfNeeded(SelectionKey key, String in) {
        String result = in;
        if (remnantsOfMessagesMap.get(key) != null) {
            result = remnantsOfMessagesMap.get(key) + in;
            remnantsOfMessagesMap.remove(key);
        }
        return result;
    }
}
