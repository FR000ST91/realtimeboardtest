package realTimeBoardTest;

public interface ChatResponseHandler {
    void onMessage(String message);
}
