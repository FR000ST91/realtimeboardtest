package realTimeBoardTest;

import com.google.common.collect.Lists;

import java.nio.channels.SelectionKey;
import java.util.List;

abstract class TcpCommand extends BaseCommand implements FinalStrings{
    private final String regex;

    TcpCommand(String commandName, String description, int parameterCol) {
        super(commandName, description);
        regex = String.format(TCP_COMMAND_REGEX, commandName, parameterCol);
    }


    @Override
    String getResult(byte[] in, SelectionKey key) {
        String inString = new String(in);
        List<String> params = Lists.newArrayList(inString.split(SPACE));
        params.remove(0);
        return bodyCommand(params, key) + DELIMITER;
    }

    @Override
    boolean isThisCommand(byte[] in) {
        String inString = new String(in);
        return inString.substring(0, inString.length() - 2).matches(regex);
    }
}
